module.exports = {
  reporters: [
    "default",
    // 'jest-junit' to enable GitLab unit test report integration
    [
      "jest-junit",
      {
        outputDirectory: "reports",
        outputName: "node-test.xunit.xml",
      },
    ],
    // 'jest-sonar' to enable SonarQube unit test report integration
    // [
    //   "jest-sonar",
    //   {
    //     outputDirectory: "reports",
    //     outputName: "node-test.sonar.xml",
    //   },
    // ],
  ],
  coverageDirectory: "reports",
  coverageReporters: [
    // 'text' to let GitLab grab coverage from stdout
    "text",
    // 'cobertura' to enable GitLab test coverage visualization
    "cobertura",
    // 'lcovonly' to enable SonarQube test coverage reporting
    // "lcovonly",
  ],
};
