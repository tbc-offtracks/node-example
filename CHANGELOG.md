## [1.0.3](https://gitlab.com/tbc-offtracks/node-example/compare/1.0.2...1.0.3) (2023-08-23)


### Bug Fixes

* declare publish registry ([eb65cb2](https://gitlab.com/tbc-offtracks/node-example/commit/eb65cb2fdf3cd12c497c51dd7e3c35c1ef36ae33))

## [1.0.2](https://gitlab.com/tbc-offtracks/node-example/compare/1.0.1...1.0.2) (2023-08-21)


### Bug Fixes

* change header case ([2a8da5a](https://gitlab.com/tbc-offtracks/node-example/commit/2a8da5a552ce041124a3dd77d04fab877003e826))

## [1.0.1](https://gitlab.com/tbc-offtracks/node-example/compare/1.0.0...1.0.1) (2023-08-16)


### Bug Fixes

* **release:** add CHENGELOG to the commited resources ([1963e7a](https://gitlab.com/tbc-offtracks/node-example/commit/1963e7a0baa9a9b0efd71ba1e33bfc8b07bd5dce))
